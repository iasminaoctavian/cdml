#ifndef INC_DOOR_LOCK_H_
#define INC_DOOR_LOCK_H_

#include <stdint.h>

#define OPEN_DOOR 	0x01
#define CLOSE_DOOR	0x02
#define DOOR_OFF	0x03

void DoorLock_Control(uint8_t Cmd);

#endif /* INC_DOOR_LOCK_H_ */
