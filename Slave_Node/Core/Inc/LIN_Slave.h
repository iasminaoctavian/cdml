/* Define to prevent recursive inclusion */
#ifndef __INC_LIN_SLAVE_H_
#define __INC_LIN_SLAVE_H_

/* C++ detection */
#ifdef __cplusplus
extern "C" {
#endif

/* Includes */
#include "stm32f1xx_hal.h"
#include "stdbool.h"

#define SYNC_FIELD			0x55
#define RX_PID_FIELD		0xEC
#define LIN_FRAME_LENGTH                    ((uint8_t) 5u)


typedef enum
{
  LINS_IDLE,
  LINS_BREAK_RECEIVED,
  LINS_WAIT_FOR_HEADER,
  LINS_HEADER_RECEIVED,
  LINS_RX_DATA,
  LINS_WAIT_FOR_RX_DATA,
  LINS_RX_DATA_RECEIVED,
  LINS_TX_DATA
} LIN_Slave_StateType;

/**
 * @brief   LIN slave error status definition
 */
typedef enum
{
  LINS_NO_ERROR,
  LINS_SYNC_ERROR,
  LINS_CHECKSUM_INVALID_ERROR
} LIN_Slave_ErrorStatusType;

LIN_Slave_StateType currentState;

/**
 * @brief   LIN Header, 2 bytes
 */
uint8_t linFrame[5];

volatile uint8_t RxDMAReceived;

uint8_t RxMessageLength;

void LIN_Slave_Process(void);

bool LIN_Data_Ready;

#ifdef __cplusplus
}
#endif

#endif /*__INC_LIN_SLAVE_H_ */
