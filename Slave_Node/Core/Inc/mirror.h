#ifndef INC_MIRROR_H_
#define INC_MIRROR_H_

#include <stdbool.h>

#define MIRROR_RIGHT	0x01
#define MIRROR_LEFT		0x02
#define MIRROR_UP		0x03
#define MIRROR_DOWN		0x04
#define MIRROR_OFF		0x05
#define MIRROR_CNT_MAX  0x09

bool MIRROR_FLAG;
uint8_t MirrorCnt;

void Mirror_Control(uint8_t Cmd);

#endif /* INC_MIRROR_H_ */
