#ifndef INC_WINDOW_H_
#define INC_WINDOW_H_

#include <stdbool.h>

#define WINDOW_UP		0x01
#define WINDOW_DOWN 	0x02
#define WINDOW_OFF		0x04
#define MAX_MEAS		0x10
#define OBJECT_DETECT	4096

bool WindowAction;
bool WindowUp;
bool ObjectDetected;

uint16_t CurrentMeas[MAX_MEAS];

void Object_Detection(void);
void Window_Control(uint8_t Cmd);

#endif /* INC_WINDOW_H_ */
