#ifndef INC_SIGNAL_H_
#define INC_SIGNAL_H_

#define SIGNAL_ON	0x00
#define SIGNAL_OFF	0x01

void SignalLight_Control(uint8_t Cmd);

#endif /* INC_SIGNAL_H_ */
