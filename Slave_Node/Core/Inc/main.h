/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "DWT_Delay.h"
#include "door_lock.h"
#include "signal.h"
#include "mirror.h"
#include "window.h"
#include "LIN_Slave.h"
#include "CmdDecoder.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define EN_A_Pin GPIO_PIN_0
#define EN_A_GPIO_Port GPIOA
#define EN_B_Pin GPIO_PIN_1
#define EN_B_GPIO_Port GPIOA
#define DoorLock_Open_Pin GPIO_PIN_12
#define DoorLock_Open_GPIO_Port GPIOB
#define DoorLock_Close_Pin GPIO_PIN_13
#define DoorLock_Close_GPIO_Port GPIOB
#define DoorLight_Pin GPIO_PIN_14
#define DoorLight_GPIO_Port GPIOB
#define IN_1_Pin GPIO_PIN_10
#define IN_1_GPIO_Port GPIOA
#define IN_2_3_Pin GPIO_PIN_11
#define IN_2_3_GPIO_Port GPIOA
#define IN_4_Pin GPIO_PIN_12
#define IN_4_GPIO_Port GPIOA
#define IN_A_Pin GPIO_PIN_6
#define IN_A_GPIO_Port GPIOB
#define IN_B_Pin GPIO_PIN_7
#define IN_B_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
