#include <stdint.h>
#include "stm32f1xx.h"
#include "main.h"
#include "CmdDecoder.h"

/**
 * These commands correspond to L298N - mirror connections:
 * OUT_1: YELLOW
 * OUT_2: RED
 * OUT_3: RED
 * OUT_4: GREEN
 */
void Mirror_Control(uint8_t Cmd)
{
	switch(Cmd)
	{
		case MIRROR_RIGHT:
		{
			HAL_GPIO_WritePin(EN_B_GPIO_Port, EN_B_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(EN_A_GPIO_Port, EN_A_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(IN_1_GPIO_Port, IN_1_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(IN_2_3_GPIO_Port, IN_2_3_Pin, GPIO_PIN_SET);
			MIRROR_FLAG = true;
			MirrorCnt = 0;
			SET_BIT(TIM2->CR1, TIM_CR1_CEN);
			break;
		}
		case MIRROR_LEFT:
		{
			HAL_GPIO_WritePin(EN_B_GPIO_Port, EN_B_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(EN_A_GPIO_Port, EN_A_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(IN_1_GPIO_Port, IN_1_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(IN_2_3_GPIO_Port, IN_2_3_Pin, GPIO_PIN_RESET);
			MIRROR_FLAG = true;
			MirrorCnt = 0;
			SET_BIT(TIM2->CR1, TIM_CR1_CEN);
			break;
		}
		case MIRROR_DOWN:
		{
			HAL_GPIO_WritePin(EN_A_GPIO_Port, EN_A_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(EN_B_GPIO_Port, EN_B_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(IN_4_GPIO_Port, IN_4_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(IN_2_3_GPIO_Port, IN_2_3_Pin, GPIO_PIN_RESET);
			MIRROR_FLAG = true;
			MirrorCnt = 0;
			SET_BIT(TIM2->CR1, TIM_CR1_CEN);
			break;
		}
		case MIRROR_UP:
		{
			HAL_GPIO_WritePin(EN_A_GPIO_Port, EN_A_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(EN_B_GPIO_Port, EN_B_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(IN_4_GPIO_Port, IN_4_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(IN_2_3_GPIO_Port, IN_2_3_Pin, GPIO_PIN_SET);
			MIRROR_FLAG = true;
			MirrorCnt = 0;
			SET_BIT(TIM2->CR1, TIM_CR1_CEN);
			break;
		}
		case MIRROR_OFF:
		{
			HAL_GPIO_WritePin(EN_A_GPIO_Port, EN_A_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(EN_B_GPIO_Port, EN_B_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(IN_1_GPIO_Port, IN_1_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(IN_4_GPIO_Port, IN_4_Pin, GPIO_PIN_RESET);
			MIRROR_FLAG = true;
			MirrorCnt = 0;
			HAL_GPIO_WritePin(IN_2_3_GPIO_Port, IN_2_3_Pin, GPIO_PIN_RESET);
			break;
		}
		default :
		{
			break;
		}
	}
}
