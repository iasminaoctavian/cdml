#include "main.h"
#include "stm32f1xx.h"

extern TIM_HandleTypeDef htim2;

void DoorLock_Control(uint8_t Cmd)
{
	if(OPEN_DOOR == Cmd)
	{
		HAL_GPIO_WritePin(DoorLock_Open_GPIO_Port, DoorLock_Open_Pin, GPIO_PIN_SET);
		/* Turn ON TIMER2 to count 30ms for the door lock pulse */
		SET_BIT(TIM2->CR1, TIM_CR1_CEN);
		/* Turn ON door light */
		HAL_GPIO_WritePin(DoorLight_GPIO_Port, DoorLight_Pin, GPIO_PIN_SET);
	}
	else if (CLOSE_DOOR == Cmd)
	{
		HAL_GPIO_WritePin(DoorLock_Close_GPIO_Port, DoorLock_Close_Pin, GPIO_PIN_SET);
		/* Turn ON TIMER2 to count 30ms for the door lock pulse */
		SET_BIT(TIM2->CR1, TIM_CR1_CEN);
		/* Turn OFF door light */
		HAL_GPIO_WritePin(DoorLight_GPIO_Port, DoorLight_Pin, GPIO_PIN_RESET);
	}
	else
	{
		/* Turn OFF TIMER2 and stop the pulse */
		CLEAR_BIT(TIM2->CR1, TIM_CR1_CEN);
		HAL_GPIO_WritePin(DoorLock_Open_GPIO_Port, DoorLock_Open_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(DoorLock_Close_GPIO_Port, DoorLock_Close_Pin, GPIO_PIN_RESET);
	}
}
