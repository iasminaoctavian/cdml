#include <stdint.h>
#include "signal.h"
#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"

extern TIM_HandleTypeDef htim3;

void SignalLight_Control(uint8_t Cmd)
{
	if(SIGNAL_ON == Cmd)
	{
		//SET_BIT(TIM3->CR1, TIM_CR1_CEN);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 549);
	}
	else
	{
		//CLEAR_BIT(TIM3->CR1, TIM_CR1_CEN);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, 0);
	}
}
