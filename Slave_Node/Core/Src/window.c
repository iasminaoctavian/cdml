#include "main.h"

extern ADC_HandleTypeDef hadc1;
extern uint16_t mean;

void Object_Detection(void)
{
	uint16_t AdcMeas = 0;
	uint8_t i;

	for(i = 0; i < MAX_MEAS; i++)
		AdcMeas += CurrentMeas[i];

	if(AdcMeas > OBJECT_DETECT)
	{
		Window_Control(WINDOW_OFF);
	}
}

void Window_Control(uint8_t Cmd)
{
	if(WINDOW_UP == Cmd)
	{
		ObjectDetected = false;
		HAL_GPIO_WritePin(IN_A_GPIO_Port, IN_A_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(IN_B_GPIO_Port, IN_B_Pin, GPIO_PIN_RESET);
		WindowAction = true;
		SET_BIT(TIM4->CR1, TIM_CR1_CEN);
		HAL_ADC_Start_DMA(&hadc1, (uint32_t*)CurrentMeas, MAX_MEAS);

	}
	else if(WINDOW_DOWN == Cmd)
	{
		HAL_GPIO_WritePin(IN_A_GPIO_Port, IN_A_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(IN_B_GPIO_Port, IN_B_Pin, GPIO_PIN_SET);
		if(ObjectDetected == true)
		{
			TIM4->ARR = 2195;
			ObjectDetected = false;
		}
		else
		{
			WindowAction = true;
			TIM4->ARR = 549;
		}
		SET_BIT(TIM4->CR1, TIM_CR1_CEN);

	}
	else
	{
		WindowAction = false;
		HAL_GPIO_WritePin(IN_A_GPIO_Port, IN_A_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(IN_B_GPIO_Port, IN_B_Pin, GPIO_PIN_RESET);
		if(ObjectDetected == true){
			ObjectDetected = false;
			Window_Control(WINDOW_DOWN);
		}
	}
}
