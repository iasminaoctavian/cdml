/* Includes */
#include <stddef.h>
#include "LIN_Slave.h"
#include "CmdDecoder.h"
#include "stm32f1xx_hal.h"

volatile int counter = 0;

/* Private typedef -----------------------------------------------------------*/

extern UART_HandleTypeDef huart3;
extern uint8_t linFrame[5];

/* Private define ------------------------------------------------------------*/
/**
 * @brief   LIN Header Length
 */

/* Private macro -------------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/**
 * @brief   LIN slave current error status
 */
static LIN_Slave_ErrorStatusType currentErrorStatus = LINS_NO_ERROR;
volatile uint8_t RxDMAReceived = 0;

/* Private function prototypes -----------------------------------------------*/


static uint8_t LIN_Checksum(uint8_t* data, uint8_t size)
{
	uint8_t CheckSum = 0;

	for(int i = 0; i < (size - 1); i++)
		CheckSum += data[i];

	CheckSum = ~CheckSum;

	return CheckSum;
}

/**
 * @brief   LIN slave node task
 * @note    This function shall be called periodically.
 * @param   None
 * @retval  None
 */
void LIN_Slave_Process(void)
{
	uint8_t CheckSum = 0;

  /* Check current LIN slave state */
  switch (currentState)
  {
    case LINS_IDLE:
      /* Wait for LIN break */
      break;

    case LINS_BREAK_RECEIVED:
      /* Start DMA for LIN header reception */
    	HAL_UART_Receive_DMA(&huart3, linFrame, LIN_FRAME_LENGTH);
      /* Wait for LIN header */
      currentState = LINS_WAIT_FOR_HEADER;

      break;

    case LINS_WAIT_FOR_HEADER:
      /* Wait for LIN header */
     if(1 == RxDMAReceived)
     {
       /* New DMA data was received, reset DMA flag */
       RxDMAReceived = 0;
       /* Check sync field */
		if(SYNC_FIELD == linFrame[0])
		{
		  /* Sync is OK, Check PID */
		  /* We expecting only one frame with PID = 0xEC */
		  if(RX_PID_FIELD == linFrame[1])
		  {
			/* Decode received Cmd */
			Decode_Cmd(linFrame[2], linFrame[3]);

			linFrame[2] = 0x00;
			linFrame[3] = 0x00;

			//while(1);
			currentState = LINS_IDLE;
		  }
		}
		else
		{
		  /* Sync field not OK, go to idle */
		  currentErrorStatus = LINS_SYNC_ERROR;
		  currentState = LINS_IDLE;
		}
     }

      break;

    default:
      break;

  }

}






