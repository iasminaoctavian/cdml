#include "CmdDecoder.h"
#include "DWT_Delay.h"
#include "mirror.h"
#include "window.h"
#include "door_lock.h"
#include "signal.h"

void Decode_Cmd(uint8_t Categ, uint8_t Cmd)
{
    switch(Categ)
    {
        case MIRROR:
        {
            if(LEFT == Cmd)
            {
                Mirror_Control(MIRROR_LEFT);
            }
            else if(RIGHT == Cmd)
            {
                Mirror_Control(MIRROR_RIGHT);
            }
            else if(UP == Cmd)
            {
                Mirror_Control(MIRROR_UP);
            }
            else if(DOWN == Cmd)
            {
                Mirror_Control(MIRROR_DOWN);
            }
            break;
        }
        case WINDOW:
        {
            if(OPEN == Cmd)
            {
                Window_Control(WINDOW_UP);
            }
            else if(CLOSE == Cmd)
            {
                Window_Control(WINDOW_DOWN);
            }
            break;
        }
        case DOOR:
        {
            if(LOCK == Cmd)
            {
                DoorLock_Control(CLOSE_DOOR);
            }
            else if(UNLOCK == Cmd)
            {
                DoorLock_Control(OPEN_DOOR);
            }
            break;
        }
        case SIGNAL_LIGHT:
        {
            if(ON == Cmd)
            {
                SignalLight_Control(SIGNAL_ON);
            }
            else if(OFF == Cmd)
            {
                SignalLight_Control(SIGNAL_OFF);
            }
            break;
        }
        default: break;
    }
}
