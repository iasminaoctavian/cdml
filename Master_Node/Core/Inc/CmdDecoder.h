#include <stdint.h>

#define MIRROR          0x01
#define WINDOW          0x03
#define DOOR            0x02
#define SIGNAL_LIGHT    0x04

/* Mirror Cmds */
#define LEFT            0x01
#define RIGHT           0x02
#define UP              0x03
#define DOWN            0x04
/* Door Cmds */
#define UNLOCK          0x05
#define LOCK            0x06
/* Window Cmds */
#define OPEN            0x07
#define CLOSE           0x08
/* Signal Light Cmds */
#define ON              0x09
#define OFF             0x0A

void Decode_Cmd(uint8_t Categ, uint8_t Cmd);
void LCD_DecodeCmd(uint8_t Categ, uint8_t Cmd);