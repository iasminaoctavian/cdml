#ifndef INC_USART_H_
#define INC_USART_H_

/**
 * @brief   Maximum USART reception buffer length
 */
#define MAX_BUFFER_LENGTH                     ((uint32_t) 2u)

/**
 * @brief   USART1 RX message buffer
 */
uint8_t RxBuffer[MAX_BUFFER_LENGTH];
uint8_t RxDMABuffer[MAX_BUFFER_LENGTH];
uint8_t RxMessageLength;
uint8_t Ready;


#endif /* INC_USART_H_ */
