#include <stdint.h>
#include <stdbool.h>

#define WINDOW_DOWN_MSK     0x01
#define WINDOW_UP_MSK       0x02
#define DOOR_LOCK_MSK       0x04
#define SIGNAL_LIGHT_MSK    0x08
#define MIRROR_RIGHT_MSK    0x10
#define MIRROR_UP_MSK       0x20
#define MIRROR_LEFT_MSK     0x40
#define MIRROR_DOWN_MSK     0x80

/* Number of checks before a switch is debounced */
#define MAX_CHECKS 10

/* Array that maintains the debounce state of every switch */
/* if (Debounced_KeyPressed[button] == true) => button was pressed */
uint8_t Debounced_KeyPressed;

/* Array that maintain the bounce status foe every button */
uint8_t State[MAX_CHECKS];

/* Pointer into State */
uint8_t Index;

/* Buffer for encoded command to be sent via LIN bus */
uint8_t CmdBuffer[2];

/* Button command ready flag */
bool ButtonReady;

/** Signal_Light and Door_Lock last state 
 *  SignalLight_State - false => OFF
 *  SignalLight_State - true  => ON
 *  DoorLock_State    - false => LOCKED
 *  DoorLock_State    - true  => UNLOCKED
 * (Initial States : Siganl OFF, Door Locked)
 * Must be integrated with the Cmds from GUI
*/
bool SignalLight_State;
bool DoorLock_State;

uint8_t RawKeyPressed();
void DebounceButtons();
void EncodeCmd(uint8_t ButtonValue);
void UpdateFlags(uint8_t Categ, uint8_t Cmd);
