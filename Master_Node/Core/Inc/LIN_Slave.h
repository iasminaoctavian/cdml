/* Define to prevent recursive inclusion */
#ifndef __INC_LIN_SLAVE_H_
#define __INC_LIN_SLAVE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f1xx_hal.h"

#define SYNC_FIELD			0x55
#define TX_PID_FIELD		0xEC
#define RX_PID_FIELD		0x6C

typedef enum
{
  LINS_IDLE,
  LINS_BREAK_RECEIVED,
  LINS_WAIT_FOR_HEADER,
  LINS_HEADER_RECEIVED,
  LINS_RX_DATA,
  LINS_WAIT_FOR_RX_DATA,
  LINS_RX_DATA_RECEIVED,
  LINS_TX_DATA
} LIN_Slave_StateType;

/**
 * @brief   LIN slave error status definition
 */
typedef enum
{
  LINS_NO_ERROR,
  LINS_SYNC_ERROR,
  LINS_CHECKSUM_INVALID_ERROR
} LIN_Slave_ErrorStatusType;

LIN_Slave_StateType currentState;

/**
 * @brief   LIN Header, 2 bytes
 */
uint8_t linHeader[2];

/**
 * @brief   LIN Header For TX, 2 bytes
 */
uint8_t linHeader_TX[2];

/**
 * @brief   LIN Data, 3 bytes
 */
uint8_t linData[3];

uint8_t RxDMAReceived;

void LIN_Slave_Process(void);
void LIN_Master_Process(uint8_t data0, uint8_t data1);

/* C++ detection */
#ifdef __cplusplus
}
#endif

#endif /*__INC_LIN_SLAVE_H_ */
