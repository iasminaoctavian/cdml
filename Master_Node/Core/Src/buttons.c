#include "buttons.h"
#include "stm32f1xx_hal.h"
#include "CmdDecoder.h"

extern uint8_t Index;

/* This function reads the key state from the hardware. */
uint8_t RawKeyPressed()
{   
    uint8_t RetVal = 0x00;

    uint8_t x1, x2, x3, x4, x5, x6, x7, x8;

    x1 = (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) & 0x01);
    x2 = (uint8_t)((HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_1) & 0x01) << 1);
    x3 = (uint8_t)((HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_2) & 0x01) << 2);
    x4 = (uint8_t)((HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_3) & 0x01) << 3);
    x5 = (uint8_t)((HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_4) & 0x01) << 4);
    x6 = (uint8_t)((HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_5) & 0x01) << 5);
    x7 = (uint8_t)((HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_6) & 0x01) << 6);
    x8 = (uint8_t)((HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_7) & 0x01) << 7);

    RetVal = (~(x1 | x2 | x3 | x4 | x5 | x6 | x7 | x8) & 0xFF);

    return RetVal;
}

/* This function performs a debouce algorithm on all buttons */
void DebounceButtons()
{
    uint8_t i, j;

    State[Index] = RawKeyPressed();
    ++Index;
    
    j = 0xFF;

    for(i = 0; i < MAX_CHECKS; i++)
    {
        j = j & State[i];
    }
    Debounced_KeyPressed = j;

    if(Debounced_KeyPressed)
    {
    	EncodeCmd(Debounced_KeyPressed);
    	Debounced_KeyPressed = 0;

    	for(i = 0; i < MAX_CHECKS; i++)
    	{
    		State[i] = 0;
    	}
    }
}

void EncodeCmd(uint8_t ButtonValue)
{
    
    if(ButtonValue & WINDOW_UP_MSK)
    {
        CmdBuffer[1] = WINDOW;
        CmdBuffer[0] = OPEN;
    }
    else if(ButtonValue & WINDOW_DOWN_MSK)
    {
        CmdBuffer[1] = WINDOW;
        CmdBuffer[0] = CLOSE;
    }
    else if(ButtonValue & DOOR_LOCK_MSK)
    {
        /* Door is currently locked */
        if(DoorLock_State == false)
        {
            /* Set Cmd to UNLOCK the door */
            CmdBuffer[1] = DOOR;
            CmdBuffer[0] = UNLOCK;
            DoorLock_State = true;
        }
        else
        {
            /* Set Cmd to turn LOCK the door */
            CmdBuffer[1] = DOOR;
            CmdBuffer[0] = LOCK;
            DoorLock_State = false;
        }
    }
    else if(ButtonValue & SIGNAL_LIGHT_MSK)
    {
        /* Signal Light is currently OFF */
        if(SignalLight_State == false)
        {
            /* Set Cmd to turn ON the light */
            CmdBuffer[1] = SIGNAL_LIGHT;
            CmdBuffer[0] = ON;
            SignalLight_State = true;
        }
        else
        {
            /* Set Cmd to turn OFF the light */
            CmdBuffer[1] = SIGNAL_LIGHT;
            CmdBuffer[0] = OFF;
            SignalLight_State = false;
        }
    }
    else if(ButtonValue & MIRROR_RIGHT_MSK)
    {
        CmdBuffer[1] = MIRROR;
        CmdBuffer[0] = RIGHT;
    }
    else if(ButtonValue & MIRROR_UP_MSK)
    {
        CmdBuffer[1] = MIRROR;
        CmdBuffer[0] = UP;
    }
    else if(ButtonValue & MIRROR_LEFT_MSK)
    {
        CmdBuffer[1] = MIRROR;
        CmdBuffer[0] = LEFT;
    }
    else if(ButtonValue & MIRROR_DOWN_MSK)
    {
        CmdBuffer[1] = MIRROR;
        CmdBuffer[0] = DOWN;
    }

    ButtonReady = true;
}

void UpdateFlags(uint8_t Categ, uint8_t Cmd)
{
    if(DOOR == Categ)
    {
        if(UNLOCK == Cmd)
            DoorLock_State = true;
        else if(LOCK == Cmd)
            DoorLock_State = false;

    }
    else if(SIGNAL_LIGHT == Categ)
    {
        if(ON == Cmd)
            SignalLight_State = true;
        else if (OFF == Cmd)
            SignalLight_State = false;
    }
}

