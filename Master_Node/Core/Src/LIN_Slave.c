/* Includes */
#include <stddef.h>
#include "LIN_Slave.h"

/* Private typedef -----------------------------------------------------------*/

extern UART_HandleTypeDef huart3;
extern uint8_t linHeader[2];
extern uint8_t linHeader_TX[2];
extern uint8_t linData[3];
extern uint8_t RxDMAReceived;

/* Private define ------------------------------------------------------------*/

/**
 * @brief   LIN Header Length
 */
#define LIN_HEADER_LENGTH                    ((uint8_t) 2u)

/**
 * @brief   LIN data length including the checksum
 */
#define LIN_DATA_LENGTH                      ((uint8_t) 3u)

/* Private macro -------------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/**
 * @brief   LIN slave current error status
 */
static LIN_Slave_ErrorStatusType currentErrorStatus = LINS_NO_ERROR;

/**
 * @brief   DMA received flag
 */
uint8_t RxDMAReceived = 0;

/* Private function prototypes -----------------------------------------------*/

static uint8_t LIN_Checksum(uint8_t* data, uint8_t size)
{
	uint8_t CheckSum = 0;

	for(int i = 0; i < (size - 1); i++)
		CheckSum += data[i];

	CheckSum = ~CheckSum;

	return CheckSum;
}

/**
 * @brief   LIN slave node task
 * @note    This function shall be called periodically.
 * @param   None
 * @retval  None
 */
void LIN_Slave_Process(void)
{
	uint8_t CheckSum = 0;

  /* Check current LIN slave state */
  switch (currentState)
  {
    case LINS_IDLE:
      /* Wait for LIN break */
      break;

    case LINS_BREAK_RECEIVED:
      /* Start DMA for LIN header reception */
     HAL_UART_Receive_DMA(&huart3, linHeader, LIN_HEADER_LENGTH);

      /* Wait for LIN header */
      currentState = LINS_WAIT_FOR_HEADER;
      break;

    case LINS_WAIT_FOR_HEADER:
      /* Wait for LIN header */
     if(1 == RxDMAReceived)
     {
       /* New DMA data was received, reset DMA flag */
       RxDMAReceived = 0;

        /* Go to next state */
        currentState = LINS_HEADER_RECEIVED;
     }
     else
     {
       /* Do nothing */
     }
      break;

    case LINS_HEADER_RECEIVED:
      /* Check sync field */
     if(SYNC_FIELD == linHeader[0])
     {
       /* Sync is OK, Check PID */
       /* We expecting only one frame with PID = 0xEC */
       if(TX_PID_FIELD == linHeader[1])
       {
          /* PID is OK, go to RX data */
          currentState = LINS_TX_DATA;
       }
       else
       {
       	 /* PID is unknown, go to idle */
       	 currentState = LINS_IDLE;
       }
     }
     else
     {
       /* Sync field not OK, go to idle */
       currentErrorStatus = LINS_SYNC_ERROR;
       currentState = LINS_IDLE;
     }
      break;

    case LINS_TX_DATA:
       	/* Start sending the Response */
    	HAL_UART_Transmit_DMA(&huart3, linData, LIN_DATA_LENGTH);
    	currentState = LINS_IDLE;

    	break;

    default:
      break;

  }

}

void LIN_Master_Process(uint8_t data0, uint8_t data1)
{
	/* Set Sync field */
	linHeader_TX[0] = SYNC_FIELD;

	/* Set PID = 0xEC as Master PID for Transmission */
	linHeader_TX[1] = TX_PID_FIELD;

	/* Set Response to transmit */
	linData[0] = data0;
	linData[1] = data1;
	linData[2] = LIN_Checksum(linData, LIN_DATA_LENGTH);

  /* Check for DoorLock or SignalLight Cmds to update the flags */
  UpdateFlags(linData[1], linData[0]);

	/* Rest Header Frame */
	linHeader[0] = 0;
	linHeader[1] = 0;

  /* Send LIN Break */
	HAL_LIN_SendBreak(&huart3);
	/* Wait for Break Field to be sent */
	while(currentState != LINS_BREAK_RECEIVED){}

	HAL_UART_Receive_DMA(&huart3, linHeader, LIN_HEADER_LENGTH);
	HAL_UART_Transmit_DMA(&huart3, linHeader_TX, LIN_HEADER_LENGTH);

    do{
    	LIN_Slave_Process();
    }while(currentState != LINS_IDLE);


}
