#include "CmdDecoder.h"
#include "LCD16x2.h"
#include "buttons.h"
#include "DWT_Delay.h"

void LCD_DecodeCmd(uint8_t Categ, uint8_t Cmd)
{
    LCD_Clear();
    DWT_Delay_ms(1);
    LCD_Set_Cursor(1, 1);
    LCD_Write_String("Current command:");
    LCD_Set_Cursor(2, 1);
    CLEAR_BIT(TIM3->CR1, TIM_CR1_CEN);

    switch(Categ)
    {
        case MIRROR:
        {
            if(LEFT == Cmd)
            {
                LCD_Write_String("Mirror-Left");
            }
            else if(RIGHT == Cmd)
            {
                LCD_Write_String("Mirror-Right");
            }
            else if(UP == Cmd)
            {
                LCD_Write_String("Mirror-Up");
            }
            else if(DOWN == Cmd)
            {
                LCD_Write_String("Mirror-Down");
            }
            break;
        }
        case WINDOW:
        {
            if(OPEN == Cmd)
            {
                LCD_Write_String("Window-Up");
            }
            else if(CLOSE == Cmd)
            {
                LCD_Write_String("Window-Down");
            }
            break;
        }
        case DOOR:
        {
            if(LOCK == Cmd)
            {
                LCD_Write_String("Door-Locked");
            }
            else if(UNLOCK == Cmd)
            {
                LCD_Write_String("Door-Unlocked");
            }
            break;
        }
        case SIGNAL_LIGHT:
        {
            if(ON == Cmd)
            {
                LCD_Write_String("Signal Light-ON");
            }
            else if(OFF == Cmd)
            {
                LCD_Write_String("Signal Light-OFF");
            }
            break;
        }
        default: break;
    }
    SET_BIT(TIM3->CR1, TIM_CR1_CEN);
}
